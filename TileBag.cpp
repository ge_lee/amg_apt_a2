#include "TileBag.h"
#include <algorithm>   
#include <random>       
#include "Tile.h"

TileBag::TileBag() {
    this->tileBag = new vector<TilePtr>();
}

TileBag::TileBag(string tilesInString) : TileBag() {
    for (unsigned int i = 0; i < tilesInString.length(); i++) {
        addTile(new Tile(tilesInString[i]));
    }
}

TileBag::~TileBag() {
    for (unsigned int i = 0; i < tileBag->size(); ++i) {
        delete (*tileBag)[i];
    }
    tileBag->clear();
    delete tileBag;
}

void TileBag::addTile(TilePtr tile) {
    tileBag->push_back(tile);
}

void TileBag::shuffleAllTiles() {
    shuffle(tileBag->begin(), tileBag->end(), std::default_random_engine(this->seed));
}

TilePtr TileBag::drawTile() {
    TilePtr tile = nullptr;
    
    if (tileBag->size() > 0) {
        tile = (*tileBag)[tileBag->size() - 1];
        tileBag->pop_back();
    }
    return tile;
}

int TileBag::getNumTiles() {
    return tileBag->size();
}

string TileBag::toString() {
    string ret = "";

    ret += getString(seed) + "\n";

    for (TilePtr &tilePtr : *tileBag) {
        ret += tilePtr->getPatternChar();
    }
    
    ret += "\n";
    return ret;
}

// Set the random seed
void TileBag::setSeed(unsigned int seed) {
    this->seed = seed;
}