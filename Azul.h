#ifndef AZUL
#define AZUL

#include "Player.h"
#include "Game.h"
#include <iostream>
#include <vector>
#include <string>

using std::string;
using std::vector;

class Azul {
public:
    // Constructor
    Azul();

    // Run the program while user has not asked to terminate
    void run(unsigned int seed);

private:
    Game* currGame;

    // Print the menu
    void printMenu();

    // Show the credits of the program
    void showCredits();

    // Get the name of a player and creates a player and return it
    string getNewPlayerName(int playerNum);

    // Start the game and handles saving if needed
    void play();

    // Print the final result and winner
    void printGameResult(PlayerPtr winner, vector<PlayerPtr>* players, vector<int> playersScore);

    // Print the score of the player
    void printScore(PlayerPtr player, int prevScore, int currScore);

    // Save the game
    void save(string savingFileName);

    // Load the saved games and asks user to choose one
    Game* loadGame();

    // Load the game from the chosen file
    Game* loadGameFromFile(string fileName);

    // Read command from user for his/her turn
    vector<string> readValidCommand();

    // Validate input factory number from user
    bool validFactory(string inputFactory);

    // Validate input colour from user
    bool validColour(string inputColour);

    // Validate input storage row from user
    bool validStorageRow(string inputStorageRow);

    // Get user input from readLine method in util and terminate program if EOF character was given 
    string getInput();

    // Read the input until getting a valid menu option
    int getMenuOption();

};

#endif // AZUL