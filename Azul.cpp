#include "Azul.h"

Azul::Azul() {
    currGame = nullptr;
}

void Azul::run(unsigned int seed) {
    print("Welcome to Azul!\n-------------------\n");

    bool terminate = false;
    while (terminate == false) {
        printMenu();

        int option = getMenuOption();
        if (option == 1) {
            // New game
            currGame = new Game(seed);
            
            // Adding players
            for (int i = 1; i <= NUM_PLAYERS ; i++) {
                currGame->addPlayer(getNewPlayerName(i));
            }
            
            currGame -> setCurrPlayer(0);
            
            play();

        } else if (option == 2) {
            // Load game
            currGame = loadGame();
            
            if (currGame != nullptr) {
                play();
            }

        } else if (option == 3) {
            showCredits();

        } else {
            terminate = true;

            print("Goodbye\n");
        }
    }
}

void Azul::printMenu() {
    print("\n");
    print("Menu\n----\n1. New Game\n2. Load Game\n");
    print("3. Credits (Show student information)\n4. Quit\n\n");
}

void Azul::showCredits() {
    print("Name: Ali Khosravi\n");
    print("Student ID: 3788120\n");
    print("Email: s3788120@student.rmit.edu.au\n");

    print("\n");

    print("Name: Man Hou\n");
    print("Student ID: 3795574\n");
    print("Email: s3795574@student.rmit.edu.au\n");

    print("\n");

    print("Name: Ge Lee\n");
    print("Student ID: 3776409\n");
    print("Email: s3776409@student.rmit.edu.au\n");

    print("\n");
}

string Azul::getNewPlayerName(int playerNum) {
    print("Enter a name for player ");
    print(getString(playerNum));
    print(" (A string without spaces)\n\n");
    
    string validName = "";
    while (validName == "") {
        vector <string> input = separateLine(getInput());
        if (input.size() == 1) {
            validName = input[0];

        } else{
            print("Please enter a valid name (a string without any extra spaces)\n");
        }
    }

    return validName;
}

void Azul::play() {
    print("Let's Play!\n\n");

    bool gameRunning = !(currGame->hasGameFinished());

    while (gameRunning) {
        print("=== START ROUND ===\n\n");

        // While the game is running and round has not finished
        while (gameRunning && currGame->hasRoundFinished() == false) {
            print("TURN FOR PLAYER: ");
            print(currGame->getCurrPlayer()->getName());
            print("\n\n");

            // Printing all factories
            print(currGame->allFactoriesToString());

            print("Mosaic for ");
            print(currGame->getCurrPlayer()->getName());
            print(":\n");

            // Printing Board for the current player
            print(currGame->getCurrPlayer()->getBoard()->boardStatus());

            print("Please enter a command in one of the following formats:\n");
            print("1- turn <factory> <colour> <storage row(or B if you want ");
            print("to put the tiles in the Broken section)>\n");
            print("2- save <saving file name>\n\n");

            bool operationDone = false;
            while (!operationDone) {
                vector<string> input = readValidCommand();

                if (input[0] == "save") {
                    save(input[1]);
                    
                    gameRunning = false;
                    operationDone = true;

                } else {
                    int factoryNum = getNumber(input[1]);

                    char colour = input[2][0];

                    int chosenRow = -1;
                    if (input[3] == "B") {
                        // Putting into "Broken" section
                        chosenRow = 6;
                    
                    } else {
                        chosenRow = getNumber(input[3]);
                    }

                    if (currGame->turn(factoryNum, colour, chosenRow)) {
                        // switch player
                        currGame->switchCurrPlayer();

                        operationDone = true;

                        print("Turn successful!\n\n");
                    
                    } else {
                        print("Please enter a valid turn\n\n");
                    }
                }
            }
        }

        // If the round is finished
        if (gameRunning && currGame -> hasRoundFinished()) {
            print("\n=== END OF ROUND === \n\n");

            vector<int> prevScores = currGame->getAllPlayersScore();

            currGame->readyForNextRound();

            vector<int> currScores = currGame->getAllPlayersScore();

            print("Round summary:\n");
            for (int i = 0; i < currGame->getNumPlayers(); i++) {
                printScore(currGame->getPlayer(i), prevScores[i], currScores[i]);
            }
            print("\n");
        }
        
        // If the game has finished
        if (gameRunning && currGame->hasGameFinished()) {
            gameRunning = false;

            print("=== GAME OVER ===\n\n");
            PlayerPtr winner = currGame->getWinner();
            printGameResult(winner, currGame->getAllPlayers(), currGame->getAllPlayersScore());
            print("\n=================\n");
        }
    }
    delete currGame;
    currGame = nullptr;
}

void Azul::printGameResult(PlayerPtr winner, 
        vector<PlayerPtr>* players, vector<int> playersScore) {
    print("-- Game result --\n");

    if (winner != nullptr) {
        print("Player ");
        print(winner->getName());
        print(" wins!\n");
        
    } else {
        print("Tie!\n");
    }

    print("\n-- Final Score --\n");

    for (unsigned int i = 0; i < players->size(); i++) {
        print("Player ");
        print((*players)[i]->getName());
        print(": ");
        print(getString(playersScore[i]));
        print("\n");
    }

    print("\n");
}

void Azul::printScore(PlayerPtr player, int prevScore, int currScore) {
    int scoreDiff = currScore - prevScore;

    print("-- Player ");
    print(player->getName());
    print(" --\n");
    print("Score earned : ");
    print(getString(scoreDiff));
    print("\n");
    print("Total score  : ");
    print(getString(currScore));
    print("\n\n");
}

vector<string> Azul::readValidCommand() {
    bool validInput = false;
    vector<string> input;

    while (validInput == false) {
        input = separateLine(getInput());

        if (input.size() == 4 && input[0] == "turn") {
            string inputFactory = input[1];

            // Changing the colour to uppercase letter
            input[2][0] = std::toupper(input[2][0]);
            string inputColour = input[2];

            // Changing b to B in case Broken section is chosen
            input[3][0] = std::toupper(input[3][0]);
            string inputStorageRow = input[3];

            if (validFactory(inputFactory) && validColour(inputColour) 
                && validStorageRow(inputStorageRow)) {
                validInput = true;

            } else if (!validFactory(inputFactory)) {
                print("Invalid factory - Please enter turn again!\n\n");
                validInput = false;

            } else if (!validColour(inputColour)) {
                print("Invalid colour - Please enter turn again!\n\n");
                validInput = false;

            } else if (!validStorageRow(inputStorageRow)) {
                print("Invalid storage row - Please enter turn again!\n\n");
                validInput = false;

            }
        } else if (input.size() == 2 && input[0] == "save") {
            validInput = true;

        } else {
            print("Please enter a valid turn or save command\n\n");
        }
    }

    return input;
}

void Azul::save(string savingFileName) {
    std::ofstream ofs("savedGames/" + savingFileName + ".txt");

    ofs << currGame -> toString();

    ofs.close();
}

Game* Azul::loadGame() {
    print("Enter the filename from which load a game\n");

    string fileName = "";
    while (fileName == "") {
        
        vector <string> v = separateLine(getInput());
        if (v.size() == 1) {
            fileName = v[0];
        } else {
            print("Enter a filename without extra spaces\n");
        }
    }

    return loadGameFromFile(fileName);
}

Game* Azul::loadGameFromFile(string fileName) {
    std::fstream fin("savedGames/" + fileName + ".txt");

    Game* ret = nullptr;

    if (!fin.is_open()) {
        print("There is no saved game with the entered name\n");

    } else {
        string tag = readLine(fin);

        if (tag != FILETAG) {
            print("File tag is invalid\n");

        } else {
            // Seed
            int seed = getNumber(readLine(fin));

            // Bag
            string bag = readLine(fin);

            // BoxLid
            string boxlid = readLine(fin);
            
            // Factories
            string factories[NUM_FACTORIES] = {};
            for (int i = 0; i < NUM_FACTORIES; i++) {
                factories[i] = readLine(fin);
            }

            // Current Player number
            int currPlayer = getNumber(readLine(fin));

            // Number of players
            int numPlayers = getNumber(readLine(fin));
            
            PlayerPtr* players = new PlayerPtr[numPlayers];
            for (int i = 0; i < numPlayers; i++) {

                // Player information
                string playerName = readLine(fin);
                string playerScore = readLine(fin);
                string wall[GRID_DIM] = {};
                for (int i = 0; i < GRID_DIM; i++) {
                    wall[i] = readLine(fin);
                }
                string storage[GRID_DIM] = {};
                for (int i = 0; i < GRID_DIM; i++) {
                    storage[i] = readLine(fin);
                }
                string broken = readLine(fin);

                players[i] = new Player(playerName, getNumber(playerScore), 
                                                storage, wall, broken);
            }

            // Creating the game using read information
            ret = new Game(seed, bag, boxlid, factories, numPlayers, currPlayer, players);
            
            delete[] players;
        }
    }

    fin.close();

    return ret;
}

bool Azul::validFactory(string inputFactory) {
    int val = getNumber(inputFactory);
    bool valid = false;

    if (inRange(val, 0, NUM_FACTORIES - 1)) {
        valid = true;
    }

    return valid;
}

bool Azul::validColour(string inputColour) {
    bool valid = false;

    int numColours = 5;
    char colours[] = {'L', 'R', 'B', 'Y', 'U'};

    for (int i = 0; i < numColours && !valid; i++) {
        if (inputColour.length() == 1 && inputColour[0] == colours[i]) {
            valid = true;
        }
    }

    return valid;
}

bool Azul::validStorageRow(string inputStorageRow) {
    int val = getNumber(inputStorageRow);
    bool valid = false;

    if (inRange(val, 1, GRID_DIM) || inputStorageRow == "B") {
        valid = true;
    }

    return valid;
}

string Azul::getInput() {
    string input = readLine();

    if (input == AMG_EOF) {
        print("Goodbye\n");

        if (currGame != nullptr){
            delete currGame;

            currGame = nullptr;
        }

        exit(0);
    }

    return input;
}

int Azul::getMenuOption() {
    int input = getNumber(getInput());

    while (!inRange(input, 1, 4)) {
        print("Enter a valid menu option\n\n");
        input = getNumber(getInput());
    }

    return input;
}
