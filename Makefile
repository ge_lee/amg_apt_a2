.default: all

all: asm2

clean:
	rm -f asm2 *.o
asm2: Azul.o Board.o BoxLid.o driver.o Factory.o Game.o LinkedList.o Player.o Tile.o TileBag.o util.o
	g++ -Wall -Werror -std=c++14 -g -O -o $@ $^

%.o:%.cpp
	g++ -Wall -Werror -std=c++14 -g -O -c $^