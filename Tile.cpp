#include "Tile.h"
#include "util.h"
#include <stdexcept>

Tile::Tile(char c) {
    if (c == 'R') {
        this->pattern = RED;
    } else if (c == 'Y') {
        this->pattern = YELLOW;
    } else if (c == 'B') {
        this->pattern = DARK_BLUE;
    } else if (c == 'L') {
        this->pattern = LIGHT_BLUE;
    } else if (c == 'U') {
        this->pattern = BLACK;
    } else if (c == 'F') {
        this->pattern = FIRST;
    } else {
        // Throw an exception if the colour is incorrect
        throw std::invalid_argument("Invalid colour.");
    }
}

Tile::Tile(const Tile& other):
    pattern(other.pattern)
{}

Tile::~Tile() {};

Pattern Tile::getPattern() const {
    return this->pattern;
}

char Tile::getPatternChar() const {
    return numberToColour(this->getPattern());
}