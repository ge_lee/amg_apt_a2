#include "LinkedList.h"
#include <exception>

Node::Node(){}

Node::~Node(){
    delete tile;
}

TilePtr Node::getTile(){
    return tile;
}

Node* Node::getNext(){
    return this->next;
}

void Node::setNext(Node* node){
    this->next = node;
}

void Node::setTile(TilePtr tile){
    this->tile = tile;
}

LinkedList::LinkedList() {
    head = nullptr;
    size = 0;
}

LinkedList::LinkedList(const LinkedList& other) {
    // No need to implement copy constructor.
}

LinkedList::~LinkedList() {
    clear();
}

unsigned int LinkedList::getSize() const {
    return size;
}

TilePtr LinkedList::get(unsigned int index) const {
    Node* current = head;
    if (index < getSize()) {
        unsigned int counter = 0;
        while (counter < index) {
            ++counter;
            current = current->getNext();
        }

    } else {
        // Throw exception when the index is bigger than the size
        throw std::out_of_range("Index too big");
    }
    return current->getTile();
}

void LinkedList::addFront(TilePtr tile) {
    Node* newNode = new Node();
    
    newNode->setTile(tile);
    newNode->setNext(this->head);
    
    this->head = newNode;
    this->size += 1;
}

void LinkedList::addBack(TilePtr tile) {
    Node* newNode = new Node();

    newNode->setTile(tile);
    newNode->setNext(nullptr);

    if (this->head == nullptr) {
        head = newNode;
    } else {
        Node* current = head;
        while(current->getNext() != nullptr) {
            current = current->getNext();
        }
        current->setNext(newNode);
    }
    this->size += 1;
}

void LinkedList::removeFront() {
    Node* current = head->getNext();
    delete head;
    head = current;
    this->size -= 1;
}

void LinkedList::clear() {
    while (head != nullptr) {
        removeFront();
    }
}

Node* LinkedList::getHead() {
    return head;
}

string LinkedList::toString() {
    string ret = "";

    Node* node = head;
    while (node != nullptr) {
        ret += node->getTile()->getPatternChar();
        node = node->getNext();
    }

    ret += "\n";
    return ret;
}