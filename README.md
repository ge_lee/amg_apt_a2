## Instructions on how to use the program

Please type the following command to compile the program:
    make

It will create an executable file called asm2

The command:
    make clean
will delete all the .o files and the asm2 file

If you want to run the program with a specific seed, use the following command:
    ./asm2 -s <seed number>

If the command you input is not the comand above *(e.g. you do not input an number or input a negative number)*,
the program will run with the built-in seed.

Otherwise, run the program using the following command *(the program will use the built-in seed)* :
    ./asm2

Only files in **savedGames** directory can be loaded into the program,
and files will be saved in that directory as well
*(note that '.txt' is not required when entering the file name for loading and saving)*

## Test files

Input test files are located in **savedGames** directory

Files with commands representing user's moves are located in **command** directory

Expected output files are located in **expectedOutput** directory
*(note that some files only show the result as the game is ended and can't be saved as output)*