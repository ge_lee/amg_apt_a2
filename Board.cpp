#include "Board.h"
#include <iostream>
#include "util.h"

Board::Board() {
    // Initialize the broken
    this->broken = new LinkedList();

    // Initialize the storage
    storage = new TilePtr*[GRID_DIM];
    for (int i = 0; i < GRID_DIM; ++i){
        storage[i] = new TilePtr[i + 1];
        for (int j = 0; j <= i; ++j) {
            storage[i][j] = nullptr;
        }
    }

    // Initialize the wall
    wall = new TilePtr*[GRID_DIM];
    for (int i = 0; i < GRID_DIM; ++i) {
        wall[i] = new TilePtr[GRID_DIM];
        for (int j = 0; j < GRID_DIM; ++j) {
            wall[i][j] = nullptr;
        }
    }
    // Initialize the score
    this->score = 0;
}

Board::Board(int score, string* storageInString, string* wallInString, string broken) {
    // Initialize the broken from file
    this->broken = new LinkedList();
    for (unsigned int i = 0; i < broken.length(); i++) {
        this->broken->addBack(new Tile(broken[i]));
    }

    // Initialize the storage from file
    storage = new TilePtr*[GRID_DIM];
    for (int i = 0; i < GRID_DIM; ++i) {
        storage[i] = new TilePtr[i + 1];

        for (int j = 0; j <= i; ++j) {
            if (storageInString[i][j] == '.') {
                storage[i][j] = nullptr;
            } else {
                storage[i][j] = new Tile(storageInString[i][j]);
            }
        }
    }

    // Initialize the wall from file
    wall = new TilePtr*[GRID_DIM];
    for (int i = 0; i < GRID_DIM; ++i) {
        wall[i] = new TilePtr[GRID_DIM];

        for (int j = 0; j < 5; ++j) {
            if (wallInString[i][j] == '.') {
                wall[i][j] = nullptr;
            } else {
                wall[i][j] = new Tile(wallInString[i][j]);
            }
        }
    }
    // Initialize the score from file
    this->score = score;
}

Board::~Board() {
    delete broken;

    for (int i = 0; i < GRID_DIM; ++i) {
        for (int j = 0; j <= i; ++j) {
            delete storage[i][j];
        }
        delete[] storage[i];
    }
    delete[] storage;

    for (int i = 0; i < GRID_DIM; ++i) {
        for (int j = 0; j < GRID_DIM; ++j) {
            delete wall[i][j];
        }
        delete[] wall[i];
    }
    delete[] wall;
}

string Board::boardStatus() {
    int i, j, m, n;
    string layout = "Board:\n";

    for (i = 1; i <= 5; ++i) {
        layout += std::to_string(i) + ":";

        for (j = GRID_DIM; j > i; --j) {
            // This is space
            layout += " ";
            // This is table cell
            layout += " ";
        }
        for (m = i - 1; m >= 0; --m) {
            // This is space
            layout += " ";
            // This is table cell
            if (storage[i - 1][m] == nullptr) {
                layout += ".";
            } else {
                layout += storage[i - 1][m]->getPatternChar();
            }
        }

        layout += " ||";
        for (n = 0; n < GRID_DIM; ++n) {
            // This is space
            layout += " ";
            // This is table cell
            if (wall[i - 1][n] == nullptr) {
                layout += ".";
            } else {
                layout += wall[i - 1][n]->getPatternChar();
            }
        }
        layout += "  " + printPatternOnTheWall(i-1);
        layout.append("\n");
    }
    layout.append(brokenToString());
    layout += "\n";

    return layout;
}

string Board::toString() {
    string ret = "";
    ret += getString(score) + "\n";

    for (int i = 0; i < GRID_DIM; ++i) {
        for (int j = 0; j < GRID_DIM; ++j) {
            
            if (wall[i][j] != nullptr) {
                ret += wall[i][j]->getPatternChar();
            } else {
                ret += '.';
            }
        }
        ret += "\n";
    }

    for (int i = 0; i < GRID_DIM; ++i) {
        for (int j = 0; j <= i; ++j) {
            
            if (storage[i][j] != nullptr) {
                ret += storage[i][j]->getPatternChar();
            } else {
                ret += '.';
            }
        }
        ret += "\n";
    }
    ret += broken->toString();
    return ret;
}

bool Board::addToStorage(int row, TilePtr tilePtr) {
    bool result = false;

    // Adding the tile to the first avalaible spot on the storage and return result
    for (int i = 0; i < row && !result; ++i) {
        if (storage[row - 1][i] == nullptr) {
            storage[row - 1][i] = tilePtr;
            result = true;
        }
    }
    return result;
}

void Board::addToBroken(TilePtr tilePtr) {
    // Always put 'first' tile at the beginning
    if (tilePtr->getPattern() == FIRST) {
        broken->addFront(tilePtr);
    } else {
        // Put other tiles at the end
        broken->addBack(tilePtr);
    }   
}

bool Board::firstMoveNextRound() {
    bool result = false;

    // Check if the first element in the broken is 'first' tile
    if (broken->getHead() != nullptr && broken->getHead()->getTile()->getPattern() == FIRST) {
        result = true;
    }
    return result;
}

vector<TilePtr> Board::moveTilesAndCalculateScore() {
    vector<TilePtr> tilesToBoxLid;
    Node* current = this->broken->getHead();

    // Iterate through the storage.
    for (int row = 1; row <= GRID_DIM; ++row) {
        // Check if the row is full
        if (isStorageFull(row)) {
            // Move the first tile to the corresponding position on the wall 
            // and get the column index on the wall 
            int colIndex = moveToWall(row, storage[row - 1][0]);

            // Set the pointer in storage to null
            storage[row - 1][0] = nullptr;  

            /**
             * Calculate the score when adding new tile to the wall
             * Calculate vertical and horizontal separately as the center tile will be 
             * calculated twice if it has adjacency tiles in both directions
             **/
            int verticalCount = verticalCounting(row - 1, colIndex);
            int horizonCount = horizonCounting(row - 1, colIndex);
            if (verticalCount > 1 && horizonCount > 1) {
                // Has adjacency tiles in both directions
                addScore(verticalCount + horizonCount);
            } else if (verticalCount > horizonCount) {
                // Only has vertical adjacency
                addScore(verticalCount);
            } else {
                // Only has horizontal adjacency
                addScore(horizonCount);
            }
                   
            // Then put the rest of the tiles to vector
            for (int i = 1; i < row; ++i) {
                tilesToBoxLid.push_back(storage[row - 1][i]);
                // Set the storage to nullptr
                storage[row - 1][i] = nullptr;
            }           
        }
    }
    // Deduct score if there is any tile on the broken
    applyingBroken();

    // Put the tiles on broken to vector
    while (current != nullptr) {
        tilesToBoxLid.push_back(current->getTile());
        current = current->getNext();
    }

    // Set the current to head before another loop
    current = broken->getHead();

    // Manually set every TilePtr to null, then call clear method.
    // Otherwise, the clear method will delete the tile object it pointed to.
    while (current != nullptr) {
        current->setTile(nullptr);
        current = current->getNext();
    }
    broken->clear();

    return tilesToBoxLid;
}

bool Board::isGameEnd() {
    bool isGameEnd = false;

    // Check rows
    for (int i = 0; i < GRID_DIM; ++i) {
        // Start from the first element of each row, if it equals to 5 meaning the row is full
        if (horizonCounting(i, 0) == GRID_DIM) {
            isGameEnd = true;
        }
    }
    return isGameEnd;
}

void Board::calculateBonusScore() {
    // Check rows
    for (int i = 0; i < GRID_DIM; ++i) {
        // Start from the first element of every row
        if (horizonCounting(i, 0) == GRID_DIM) {
            // Add 2 additional score if each row on the wall is fully filled
            addScore(2);
        }
    }

    // Check columns
    for (int i = 0; i < GRID_DIM; ++i) {
        // Start from the first element of every column
        if (verticalCounting(0, i) == GRID_DIM) {
            // Add 7 additional score if each column on the wall is fully filled
            addScore(7);
        }
    }

    // Check patterns
    for (int i = 0; i < GRID_DIM; ++i) {
        // Start from the first row and check the same colour.
        if (patternCounting(i) == GRID_DIM) {
            // Add 10 additional score if each colour on the wall is fully filled
            addScore(10);
        }
    }
}

int Board::tilesInStorage(int row) {
    int numberOfTiles = 0;

    for (int i = 0; i < row; ++i) {
        if (storage[row - 1][i] != nullptr) {
            ++numberOfTiles;
        }
    }
    return numberOfTiles;
}

bool Board::colourInStorage(int row, Pattern pattern) {
    bool result = true;
    
    if (storage[row - 1][0] != nullptr && storage[row - 1][0]->getPattern() != pattern) {
        result = false;
    }
    return result;
}

bool Board::isWallFilled(int row, Pattern pattern) {
    bool result = false;

    if (wall[row - 1][(pattern + row - 1) % GRID_DIM] != nullptr) {
        result = true;
    }
    return result;
}

bool Board::isStorageFull(int row) {
    bool result = true;

    for (int i = 0; i < row; ++i) {
        if (storage[row - 1][i] == nullptr) {
            result = false;
        }
    }
    return result;
}

int Board::getScore() {
    return score;
}

void Board::addScore(int score) {
    this->score += score;
}

// The methods below are private methods

int Board::verticalCounting(int rowIndex, int colIndex) {
    int ret = 0;
    
    // Count to the bottom
    for (int i = 0; rowIndex + i < GRID_DIM && wall[rowIndex + i][colIndex] != nullptr; i++) {
        ret++;
    }

    // Count to the top
    for (int i = 0; rowIndex - i >= 0 && wall[rowIndex - i][colIndex] != nullptr; i++) {
        ret++;
    }

    // Deduct because we counted the center tile twice
    ret -= (wall[rowIndex][colIndex] != nullptr);
    return ret;
}

int Board::horizonCounting(int rowIndex, int colIndex) {
    int ret = 0;

    // Count to the right
    for (int i = 0; colIndex + i < GRID_DIM && wall[rowIndex][colIndex + i] != nullptr; i++) {
        ret++;
    }

    // Count to the left
    for (int i = 0; colIndex - i >= 0 && wall[rowIndex][colIndex - i] != nullptr; i++) {
        ret++;
    }

    // Deduct because we counted the center tile twice
    ret -= (wall[rowIndex][colIndex] != nullptr);
    return ret;
}

int Board::patternCounting(int colIndex) {
    int ret = 0;
    
    for (int i = 0; i < GRID_DIM; i++) {
        ret += (wall[i][(colIndex + i) % GRID_DIM] != nullptr);
    } 
    return ret;
}

void Board::applyingBroken() {
    unsigned int size = broken->getSize();

    if (size == 0) {
        // No deduction if the broken is empty
    } else if (size == 1) {
        this->addScore(-1);
    } else if (size == 2) {
        this->addScore(-2);
    } else if (size == 3) {
        this->addScore(-4);
    } else if (size == 4) {
        this->addScore(-6);
    } else if (size == 5) {
        this->addScore(-8);
    } else if (size == 6) {
        this->addScore(-11);
    } else {
        this->addScore(-14);
    }

    // There is no negative score on the game board, the minimal score is 0
    if (this->score < 0) {
        this->score = 0;
    }
}

int Board::moveToWall(int row, TilePtr tilePtr) {
    // Formula of column index of each colour on wall = (enum value + row - 1) mod GRID_DIM
    int colIndex = (tilePtr->getPattern() + row - 1) % GRID_DIM;
    wall[row - 1][colIndex] = tilePtr;

    return colIndex;
}

string Board::brokenToString() {
    Node* current = this->broken->getHead();
    int count = 0;
    string ret = "Broken: ";
    int size = broken->getSize();

    /** Instead of calling get method in the LinkedList class inside a for loop, 
     *  we are using two loops separately, this makes sure the time complexity is O(n)
     **/
    if (size <= 7) {
        while (count < size) {
            /** The two lines below were "string += string + char" (operating string 
             *  and char at the same time), but this returned some strange characters.
             *  Same for the line 444 and 445.
             **/
            ret.append(" ");
            ret += current->getTile()->getPatternChar();
            current = current->getNext();
            ++count;
        }
        for (int i = 0; i < 7 - size; ++i) {
            ret += " .";
        }  
        
    } else {
        while (count < 7) {
            ret.append(" ");
            ret += current->getTile()->getPatternChar();
            current = current->getNext();
            ++count;
        }
    }
    ret += "\n";
    return ret;
}

string Board::printPatternOnTheWall(int rowindex){
    string ret = "";
    for(int colIndex = 0; colIndex < GRID_DIM; ++colIndex){
        ret += " ";
        ret += numberToColour((colIndex + GRID_DIM - rowindex)%GRID_DIM);
    }
    return ret;
}
