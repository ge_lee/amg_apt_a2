#include "BoxLid.h"

BoxLid::BoxLid() {
    this->boxLid = new vector<TilePtr>();
}

BoxLid::BoxLid(string tilesInString) : BoxLid() {
    for (unsigned int i = 0; i < tilesInString.length(); i++) {
        boxLid->push_back(new Tile(tilesInString[i]));
    }
}

BoxLid::~BoxLid() {
    for (unsigned int i = 0; i < boxLid->size(); ++i) {
        delete (*boxLid)[i];
    }
    boxLid->clear();
    delete boxLid;
}

void BoxLid::addTile(TilePtr tile) {
    boxLid->push_back(tile);
}

TilePtr BoxLid::drawTile() {
    TilePtr tile = nullptr;

    if (boxLid->size() > 0) {
        tile = (*boxLid)[boxLid->size() - 1];
        boxLid->pop_back();
    }
    return tile;
}

int BoxLid::getNumTiles() {
    return boxLid->size();
}

string BoxLid::toString() {
    string ret = "";

    for (TilePtr &tilePtr : *boxLid) {
        ret += tilePtr->getPatternChar();
    }
    
    ret += "\n";
    return ret;
}