#include "Game.h"

using std::swap;

Game::Game(unsigned int seed) {
    this->players = new vector<PlayerPtr>();
    this->currPlayer = nullptr;
    this->tileBag = new TileBag();
    // Set random seed
    this->tileBag->setSeed(seed);
    this->boxLid = new BoxLid();
    this->factories = new Factory*[NUM_FACTORIES];
    for (int i = 0; i < NUM_FACTORIES; ++i) {
        (this->factories)[i] = new Factory();
    }
    
    setUpNewGame();
}

Game::Game(unsigned int seed, string bag, string boxLid, string* factories, int numPlayers, 
            int currPlayer, PlayerPtr* players) { 

    this->players = new vector<PlayerPtr>();
    for (int i = 0; i < numPlayers; i++) {
        this->players->push_back(players[i]);
    }
    this->currPlayer = players[currPlayer - 1];
    this->tileBag = new TileBag(bag);
    this->tileBag->setSeed(seed);
    this->boxLid = new BoxLid(boxLid);
    this->factories = new Factory*[NUM_FACTORIES];
    for (int i = 0; i < NUM_FACTORIES; ++i) {
        (this->factories)[i] = new Factory(factories[i]);
    }
}

Game::~Game() {
    for (PlayerPtr &player : *players) {
        delete player;
        player = nullptr;
    }
    players->clear();
    delete players;
    
    delete tileBag;
    delete boxLid;

    this->currPlayer = nullptr;

    for (int i = 0; i < NUM_FACTORIES; ++i) {
        if (factories[i] != nullptr) {
            delete factories[i];
            factories[i] = nullptr;
        }
    }
    delete[] factories;
    factories = nullptr;
}

void Game::setUpNewGame() {
    char tileTypes[MAX_PATTERN_TO_PLAY] = {'L', 'B', 'Y', 'R', 'U'};

    // Initialize the tiles to be played and add them to tilebag
    for (int i = 0; i < MAX_TILE_PER_PATTERN; ++i) {
        for (int j = 0; j < MAX_PATTERN_TO_PLAY; ++j) {
            TilePtr newTile = new Tile(tileTypes[j]);
            tileBag->addTile(newTile);
        }
    }
    tileBag->shuffleAllTiles();

    // Initialize the 'first' tile
    TilePtr fTile = new Tile('F');
    fillAllFactories(fTile);
}

void Game::fillAllFactories(TilePtr fTile) {
    // Fill central factory with 'first' tile
    factories[0]->addTile(fTile);

    // Fill each factory with 4 tiles excluding the central factory
    for (int i = 1; i < NUM_FACTORIES; ++i) {
        for (int j = 0; j < MAX_TILE_PER_FACTORY; ++j) {

            // Refill the tile bag if it is empty
            if (tileBag->getNumTiles() == 0) {
                refillTileBag();
            }
            factories[i]->addTile(tileBag->drawTile());
        } 
    }
}

void Game::refillTileBag() {
    for (int i = 0; i < boxLid->getNumTiles(); ++i) {
        tileBag->addTile(boxLid->drawTile());
    }
    tileBag->shuffleAllTiles();
}

bool Game::turn(int factoryNum, char colour, int row) {
    bool validTurn = false;

    // Check if the selected factory and storage row is valid
    validTurn = ((factories[factoryNum]->hasTileColour(colour)) && validRowSelected(row, colour));
    
    if (validTurn) {
        makeTurn(factoryNum, colour, row);
    }

    return validTurn;
}

void Game::makeTurn(int factoryNum, char colour, int row) {
    vector<TilePtr>* factory = factories[factoryNum]->getTiles();

    // Check if player selects tile from central factory OR other factories
    if (factoryNum != 0) {

        // Loop through the selected factory
        for (unsigned int i = 0; i < factory->size(); i++) {

            // Check if the tile matches the tile selected by player
            if ((*factory)[i]->getPatternChar() == colour) {

                // Add tile to broken if user selects broken instead of storage OR selected 
                // storage is fully filled, OTHERWISE add tile to selected storage
                if (row == 6 || currPlayer->getBoard()->addToStorage(row, (*factory)[i]) == false) {
                    currPlayer->getBoard()->addToBroken((*factory)[i]);
                }

            } else {
                // Add the tile to central factory
                factories[0]->addTile((*factory)[i]);
            }

            (*factory)[i] = nullptr;
        }
        factory->clear(); 

    } else {
        unsigned int i = 0;

        // Loop through the central factory
        while (i < factory->size()) {

            // Check if the tile matches the tile selected by player OR it is the 'first' tile
            if ((*factory)[i]->getPatternChar() == colour || (*factory)[i]->getPatternChar() == 'F') {

                // Add tile to broken if user selects broken instead of storage OR the tile is
                // 'first OR selected storage if fully filled, OTHERWISE add tile to selected storage
                if (row == 6 || (*factory)[i]->getPatternChar() == 'F' 
                    || currPlayer->getBoard()->addToStorage(row, (*factory)[i]) == false) {
                    currPlayer->getBoard()->addToBroken((*factory)[i]);
                }
                
                swap((*factory)[i], (*factory)[factory->size() - 1]);
                (*factory)[factory->size() - 1] = nullptr;
                factory->pop_back();

            } else {
                i++;
            }
        }
    }
}

bool Game::validRowSelected(int row, char colour) {
    bool validRow = true;

    if (row != 6 && (currPlayer->getBoard()->isWallFilled(row, colourToPattern(colour)) == true || 
        currPlayer->getBoard()->colourInStorage(row, colourToPattern(colour)) == false || 
        currPlayer->getBoard()->isStorageFull(row) == true)) {

        validRow = false;
    }
    
    return validRow;
}

void Game::readyForNextRound() {
    // Set current player based on player who has the first tile
    for (PlayerPtr &player : *players) {
        if (player->getBoard()->firstMoveNextRound()) {

            currPlayer = player;
        }
    }

    // Collect all excess tiles in storage and broken of players
    vector<TilePtr> excessTiles;
    for (PlayerPtr &player : *players) {
        vector <TilePtr> temp = player->getBoard()->moveTilesAndCalculateScore();

        for (TilePtr &tile : temp) {
            excessTiles.push_back(tile);
        }

        temp.clear();
    }
    
    TilePtr fTile = nullptr;

    // Move excess tiles collected to box lid excluding the 'first' tile
    for (unsigned int i = 0; i < excessTiles.size(); ++i) {
        if (excessTiles[i]->getPatternChar() == 'F') {
            fTile = excessTiles[i];
        } else {
            boxLid->addTile(excessTiles[i]);
        }
    }

    // Fill factories for next round together with 'first' tile collected earlier
    fillAllFactories(fTile);
}

void Game::addPlayer(string playerName) {
    players->push_back(new Player(playerName));
}

PlayerPtr Game::getPlayer(int i) {
    return (*(this->players))[i];
}

PlayerPtr Game::getCurrPlayer() {
    return this->currPlayer;
}

void Game::setCurrPlayer(int playerNum) {
    this->currPlayer = (*players)[playerNum];
}

void Game::switchCurrPlayer() {
    int id = getCurrPlayerID();
    
    // Switch the current player to the next one
    id = (id + 1) % (players->size());

    currPlayer = (*players)[id];
}

vector<int> Game::getAllPlayersScore() {
    vector<int> playersScore;

    for (PlayerPtr &player : *players) {
        playersScore.push_back(player->getBoard()->getScore());
    }
    return playersScore;
}

bool Game::hasRoundFinished() {
    bool isFinished = true;

    for (int i = 0; i < NUM_FACTORIES; ++i) {
        if (factories[i]->getNumTiles() > 0) {
            isFinished = false;
        }
    }
    return isFinished;
}

bool Game::hasGameFinished() {
    bool isFinished = false;

    for (PlayerPtr &player : *players) {
        if (player->getBoard()->isGameEnd()) {
            
            isFinished = true;
        }
    }
    return isFinished;
}

PlayerPtr Game::getWinner() {
    PlayerPtr winner = nullptr;
    int maxPoint = -1;

    for (PlayerPtr &player : *players) {
        player->getBoard()->calculateBonusScore();

        int point = player->getBoard()->getScore();
        if (point > maxPoint) {
            // This player has the maximum score up to this point
            maxPoint = point;
            winner = player;

        } else if (point == maxPoint) {
            // There is a tie
            winner = nullptr;
        }
    }
    return winner;
}

string Game::allFactoriesToString() {
    string ret = "";

    ret += "Factories:\n";

    for (int i = 0; i < NUM_FACTORIES; ++i) {
        ret += getString(i);
        ret += ": " + factories[i]->toString();
    }

    ret += "\n";
    return ret;
}

string Game::toString() {
    string ret = FILETAG;
    
    ret += "\n" + tileBag->toString() + boxLid->toString();
    
    for (int i = 0; i < NUM_FACTORIES; i++) {
        ret += factories[i]->toString();
    }
    
    // Current player
    ret += getString(getCurrPlayerID() + 1);
    ret += "\n";

    // Number of players
    ret += getString(players->size());
    ret += "\n";
    
    for (PlayerPtr &player : *players) {
        ret += player->toString();
    }
    
    return ret;
}

Pattern Game::colourToPattern(char colour) {
    Pattern ret;

    if (colour == 'R'){
        ret = RED;
    } else if (colour == 'Y'){
        ret = YELLOW;
    } else if (colour == 'B'){
        ret = DARK_BLUE;
    } else if (colour == 'L'){
        ret = LIGHT_BLUE;
    } else if (colour == 'U'){
        ret = BLACK;
    } else {
        ret = FIRST;
    }
    return ret;
}

int Game::getCurrPlayerID() {
    int ret = -1;

    for (unsigned int i = 0; i < players->size(); i++) {
        if ((*players)[i] == currPlayer) {
            
            ret = i;
        }
    }
    return ret;
}

int Game::getNumPlayers() {
    return players->size();
}

vector<PlayerPtr>* Game::getAllPlayers() {
    return players;
}