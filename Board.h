#ifndef BOARD
#define BOARD

#include "LinkedList.h"
#include <vector>
#include <string>
#include "util.h"

using std::string;

class Board {
public:
    // Default constructor
    Board();

    // Constructor for loading from saved file
    Board(int score, string* storageInString, string* wallInString, string broken);

    // Destructor
    ~Board();
    
    // Return a string that is formatted for saving
    string toString();

    // Return a string that is formatted for displaying board on screen
    string boardStatus();

    // Add the given tile to storage, return false if the storage is full
    bool addToStorage(int row, TilePtr tilePtr);

    // Add the given tile to broken
    void addToBroken(TilePtr tilePtr);

    // Check if the broken has FIRST tile.
    bool firstMoveNextRound();

    // Calculate score and move the tiles from storage to wall
    // Return a collection of tilePtrs that needs to be added to boxlid
    vector<TilePtr> moveTilesAndCalculateScore();

    // Check if any of row on the wall is full
    bool isGameEnd();

    // Calculate bonus score after the game ends
    void calculateBonusScore();

    // Check if the given row on storage has the given colour
    // Return true if the row is empty or has the same colour
    bool colourInStorage(int row, Pattern pattern);

    // Check if the given row on wall if filled with the given colour
    bool isWallFilled(int row, Pattern pattern);
    
    // Check if the storage is full for the given row
    bool isStorageFull(int row);

    // Getter
    int getScore();

private:
    // Add the score. This is not setter.
    void addScore(int score);
    
    // Move the tile on the given row from storage to wall and return the column index 
    int moveToWall(int row, TilePtr tilePtr);

    // Calculate the number of vertical adjacency spot that was filled on the wall
    int verticalCounting(int rowIndex, int colIndex);

    // Calculate the number of horizontal adjacency spot that was filled on the wall
    int horizonCounting(int rowIndex, int colIndex);

    // Calculate the number of same colour spot that was filled on the wall
    int patternCounting(int colIndex);

    // Calculate the broken and deduct the score
    void applyingBroken();

    // Return a string that is formatted for displaying broken on screen
    string brokenToString();

    // Return the number of tiles on the given row on storage
    int tilesInStorage(int row);

    // The score of the board
    int score;

    // This method will return the pattern layout of given row on the wall
    string printPatternOnTheWall(int row);

    // Storage or left grid on the game board
    TilePtr** storage;

    // Wall or right grid on the game board
    TilePtr** wall;

    // Broken on the game board
    LinkedList* broken; 

};

#endif // BOARD