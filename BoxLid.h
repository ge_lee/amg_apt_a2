#ifndef BOXLID
#define BOXLID

#include "util.h"
#include "Tile.h"

class BoxLid {
public:
    // Constructor
    BoxLid();

    // Constructor for loading from file
    BoxLid(string tilesInString);

    // Destructor
    ~BoxLid();

    // Add tile into box lid
    void addTile(TilePtr tile);
    
    // Draw a tile from the box lid
    TilePtr drawTile();

    // Get the number of tiles in a box lid
    int getNumTiles();

    // Get the output infomation
    string toString();

private:
    vector<TilePtr>* boxLid;

};

#endif // BOXLID